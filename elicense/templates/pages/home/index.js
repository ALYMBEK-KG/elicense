frappe.ready(function () {
    initInteractiveMap();
});

function initInteractiveMap() {
    const map = L.map('leaflet', {
        center: L.latLng(41.84, 75.06),
        maxBounds: L.latLngBounds(
            L.latLng(44.0, 68.0),
            L.latLng(39.0, 81.0)
        ),
        maxZoom: 20,
        minZoom: 6,
        zoom: 6,
        attributionControl: false,
        layers: [
            L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            })
        ]
    });

    fetch('/assets/elicense/data/map_data.json')
        .then(r => r.json())
        .then(r => {
            L.geoJSON(r,
                {
                    onEachFeature: function (feature, layer) {
                        layer.bindPopup('<b>Информация о аптеке</b><br>#1');

                        layer.on('click', function () {
                            map.flyTo(layer.getLatLng(), map.getZoom(), {
                                animate: true,
                                duration: 0.3
                            });
                            layer.openPopup();
                        });

                    }
                }
            ).addTo(map)
        });

    fetch('/assets/elicense/data/kg_map_bounds_data.json')
        .then(r => r.json())
        .then(r => L.geoJSON(r).addTo(map));
}

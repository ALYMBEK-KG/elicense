frappe.ready(function () {
    $('.custom-dropdown-toggle').click(function (e) {
        const dropdown = $(this);
        const dropdownMenu = dropdown.next('.dropdown-menu');
        dropdownMenu.toggleClass('show');

        $('body').click(function (e) {
            if ($(e.target).closest(dropdown).length === 0 && $(e.target).closest(dropdownMenu).length === 0) {
                dropdownMenu.removeClass('show');
            }
        });
    });
});

frappe.ready(function () {
    frappe.session.logout = () => {
        return frappe.call({
            method: "logout",
            callback: function (r) {
                if (r.exc) return;
                window.location.href = '/';
            }
        });
    }

    frappe.session.set_lang = async (lang = 'kg') => {
        const checkedLang = lang == null ? 'kg' : lang;
        document.cookie = `preferred_language=${checkedLang}`;

        const reload = function () {
            window.location.reload();
        }

        if (frappe.session.user != 'Guest') {
            // method: frappe.client.set_value or frappe.client.get_value
            frappe.call({
                method: "frappe.client.set_value",
                args: {
                    doctype: 'User',
                    name: frappe.session.user,
                    fieldname: 'language',
                    value: checkedLang,
                },
                callback: reload
            });
        } else {
            reload();
        }
    }

    $(document).ready(function () {
        const lang = frappe.get_cookie("preferred_language");
        if (lang == null) frappe.session.set_lang(lang);
        $('.dropdown.language-switcher > .dropdown-menu > .dropdown-item').each(function (i, v) {
            if ($(v).attr('onclick').includes(lang)) {
                $(v).addClass('bg-primary text-white');
            } else {
                $(v).removeClass('bg-primary text-white');
            }
        });
    });
});

## E-License

E-License module for frappe
([Frappe framework docs](https://frappeframework.com/docs/user/en/basics))

#### Installation
`bench get-app elicense https://<username>:<personal_token>@gitlab.com/ALYMBEK-KG/elicense.git`\
`bench --site <site> install-app elicense`\
`bench --site <site> migrate`

#### License

mit
